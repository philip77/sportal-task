<?php

namespace App\Controller;

use App\Entity\Article;
use App\Form\Type\ArticleType;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraints\DateTime;
use Doctrine\ORM\Tools\Pagination\Paginator;

class ArticleController extends AbstractFOSRestController
{
    const LIMIT_ARTICLES_PER_PAGE = 5;

    public function list(Request $request, EntityManagerInterface $entityManager): Response
    {
        $repository = $entityManager->getRepository(Article::class);
        $query = $repository->createQueryBuilder('a');

        $status = $request->query->get('status', 'active');

        if (! in_array($status, ['active', 'not active'])) {
            $status = 'active';
        }

        $query->where('a.status = :status');

        $createdAt = $request->query->get('created_at');

        if ($this->validateDate($createdAt)) {
            $query = $this->filterByDate($query, $createdAt, 'created_at');
        }

        $publishAt = $request->query->get('publish_at');

        if ($this->validateDate($publishAt)) {
            $query = $this->filterByDate($query, $publishAt, 'publish_at');
        }

        $query->setParameter(':status', $status);
        $page = (int) $request->query->get('page');

        if ($page) {
            $paginator = $this->paginate($query, $page);
            $articles = $paginator->getQuery()->getResult(); ;
        } else {
            $articles = $query->getQuery()->getResult();
        }

        return $this->responseByContentType($request->getContentType(), $articles);
    }

    public function createArticle(Request $request, ManagerRegistry $managerRegistry, FormFactoryInterface $formFactory)
    {
        $form = $formFactory->createNamed('', ArticleType::class, null, []);

        $form->handleRequest($request);

        if (! $form->isSubmitted() || ! $form->isValid()) {
            return $this->handleView($this->view($form, Response::HTTP_BAD_REQUEST));
        }

        /**
         * @var Article $article
         */
        $article = $form->getData();

        $dateTimeNow = new \DateTime();

        if (! $article->getCreatedAt()) {
            $article->setCreatedAt($dateTimeNow);
        }

        if (! $article->getPublishAt()) {
            $article->setPublishAt($dateTimeNow);
        }

        $managerRegistry->getManager()->persist($article);
        $managerRegistry->getManager()->flush();
        $articleArray[] = $article;

        return $this->responseByContentType($request->getContentType(), $articleArray);
    }

    private function validateDate(?string $date, string $format = 'Y-m-d'): bool
    {
        $d = \DateTime::createFromFormat($format, $date);

        return $d && $d->format($format) === $date;
    }

    private function paginate(QueryBuilder $dql, int $page = 1): Paginator
    {
        $paginator = new Paginator($dql);

        $paginator->getQuery()
            ->setFirstResult(self::LIMIT_ARTICLES_PER_PAGE * ($page - 1))
            ->setMaxResults(self::LIMIT_ARTICLES_PER_PAGE);

        return $paginator;
    }

    private function filterByDate(QueryBuilder $query, string $dateValue, string $field): QueryBuilder
    {
        $createdAtObj = new \DateTime($dateValue);
        $from = new \DateTime($createdAtObj->format("Y-m-d")." 00:00:00");
        $to   = new \DateTime($createdAtObj->format("Y-m-d")." 23:59:59");

        return $query->andWhere("a.$field BETWEEN :from_$field AND :to_$field")
            ->setParameter("from_$field", $from )
            ->setParameter("to_$field", $to);
    }

    private function xmlResponse(array $data, int $statusCode = Response::HTTP_OK)
    {
        $xml = '<?xml version="1.0" encoding="UTF-8"?>';

        /**
         * @var Article[] $data
         */
        foreach ($data as $row) {
            $xml .= '<article>';
            $xml .= "<id>{$row->getId()}</id>";
            $xml .= "<title>{$row->getTitle()}</title>";
            $xml .= "<content>{$row->getContent()}</content>";
            $xml .= "<created_at>{$row->getCreatedAt()->format('Y-m-d H:i:s')}</created_at>";
            $xml .= "<publish_at>{$row->getPublishAt()->format('Y-m-d H:i:s')}</publish_at>";
            $xml .= "<status>{$row->getStatus()}</status>";
            $xml .= '</article>';
        }

        $response = new Response($xml, $statusCode);
        $response->headers->set('Content-Type', 'text/xml');

        return $response;
    }

    private function csvResponse(array $data, int $statusCode = Response::HTTP_OK)
    {
        $fp = fopen('php://temp', 'w');

        /**
         * @var Article[] $data
         */
        foreach ($data as $row) {
            $csvOutput = [];
            $csvOutput['id'] = $row->getId();
            $csvOutput['title'] = $row->getTitle();
            $csvOutput['content'] = $row->getContent();
            $csvOutput['created_at'] = $row->getCreatedAt()->format('Y-m-d H:i:s');
            $csvOutput['publish_at'] = $row->getPublishAt()->format('Y-m-d H:i:s');
            $csvOutput['status'] = $row->getStatus();
            fputcsv($fp, $csvOutput);
        }

        rewind($fp);
        $response = new Response(stream_get_contents($fp), $statusCode);
        fclose($fp);

        $response->headers->set('Content-Type', 'text/csv');
        $response->headers->set('Content-Disposition', 'attachment; filename="testing.csv"');

        return $response;
    }

    private function responseByContentType(?string $contentType = 'json', array $data = [], int $statusCode = Response::HTTP_OK)
    {
        switch ($contentType) {
            case 'xml':
                return $this->xmlResponse($data, $statusCode);
            case 'csv':
                return $this->csvResponse($data, $statusCode);
            default:
                return $this->handleView($this->view($data, $statusCode));
        }
    }
}