<?php

namespace App\DBAL;

use Doctrine\DBAL\Types\Type;
use Doctrine\DBAL\Platforms\AbstractPlatform;

class EnumArticleStatusType extends Type
{
    const ENUM_ARTICLE_STATUS = 'enum_article_status';
    const STATUS_ACTIVE = 'active';
    const STATUS_NOT_ACTIVE = 'not active';

    public function getSQLDeclaration(array $fieldDeclaration, AbstractPlatform $platform)
    {
        return sprintf("ENUM(%s)", implode(',', ['"active"','"not active"']));
    }

    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        return $value;
    }

    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        if (! in_array($value, ['active','not active'])) {
            throw new \InvalidArgumentException("Invalid article status!");
        }

        return $value;
    }

    public function getName()
    {
        return self::ENUM_ARTICLE_STATUS;
    }

    public function requiresSQLCommentHint(AbstractPlatform $platform)
    {
        return true;
    }
}