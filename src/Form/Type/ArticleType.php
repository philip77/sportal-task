<?php

namespace App\Form\Type;

use App\Entity\Article;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotNull;

class ArticleType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('title', TextType::class, [
            'constraints' => [new NotNull(), new Length(['min' => 1, 'max' => 255])]
        ])->add('content', TextType::class, [
            'constraints' => [new NotNull(), new Length(['min' => 1, 'max' => 16353])]
        ])->add('created_at', DateTimeType::class, ['widget' => 'single_text', 'required' => false])
        ->add('publish_at', DateTimeType::class, ['widget' => 'single_text', 'required' => false])
        ->add('status', ChoiceType::class, ['choices' => [
            'active', 'not active'
        ]]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefault('data_class', Article::class);
    }

}